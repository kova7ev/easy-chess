# Easy Chess.
Simplest way for learning chess.
Sample is here [code review](https://vimeo.com/135133692) and here [demo](https://vimeo.com/135365032).
It's just prototype and I not sure what it will to work on real device or with different chess mates skins.

Screen #1
![1.png](https://bitbucket.org/repo/LKkpLK/images/1135333541-4.png)
Screen #2
![2.png](https://bitbucket.org/repo/LKkpLK/images/577911427-1.png)
Screen #3
![3.png](https://bitbucket.org/repo/LKkpLK/images/4114471093-2.png)