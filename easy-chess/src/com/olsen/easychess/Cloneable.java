package com.olsen.easychess;

public abstract class Cloneable {
	public abstract Object clone();
}
