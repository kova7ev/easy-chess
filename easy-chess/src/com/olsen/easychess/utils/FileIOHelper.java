package com.olsen.easychess.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

public class FileIOHelper {
	public static File open(Activity context, String path) {
		File file = null;

		try {
			InputStream is = context.getAssets().open(path);
			int index = path.indexOf("/");

			if (index != -1) {
				String dirname = path.substring(0, index);
				File dir = context.getDir(dirname, Context.MODE_PRIVATE);
				file = new File(dir, path.substring(index + 1));
			} else {
				file = new File(path);
			}

			FileOutputStream os = new FileOutputStream(file);

			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = is.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}

			is.close();
			os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return file;
	}

	public static File save(Activity context, String path, Bitmap bitmap) {
		FileOutputStream out = null;
		File file = null;

		try {
			int index = path.indexOf("/");

			if (index != -1) {
				String dirname = path.substring(0, index);
				File dir = context.getDir(dirname, Context.MODE_PRIVATE);
				file = new File(dir, path.substring(index + 1));
			} else {
				file = new File(path);
			}

			out = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return file;
	}

	private File save(Activity context, String path, byte[] data) {
		FileOutputStream out = null;
		File file = null;

		try {
			int index = path.indexOf("/");

			if (index != -1) {
				String dirname = path.substring(0, index);
				File dir = context.getDir(dirname, Context.MODE_PRIVATE);
				file = new File(dir, path.substring(index + 1));
			} else {
				file = new File(path);
			}

			out = new FileOutputStream(file);
			out.write(data);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return file;
	}

}
