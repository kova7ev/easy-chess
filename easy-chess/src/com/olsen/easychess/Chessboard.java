package com.olsen.easychess;

import java.util.HashMap;
import java.util.Map;

public class Chessboard<T extends Cloneable> extends Cloneable{
	private Map<String, T> mBoard;
	
	public Chessboard() {
		mBoard = new HashMap<String, T>();
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				String key = String.format("%d%d", i, j);
				mBoard.put(key, null);
			}
		}
	}
	
	public T get(int row, int column){
		String key = String.format("%d%d", row, column);
		return mBoard.get(key);
	}
	
	public void set(int row, int column, T chesspiece){
		String key = String.format("%d%d", row, column);
		mBoard.put(key, chesspiece);
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for (int i = 0; i < 8; i++){
			for (int j = 0; j< 8; j++){
				T piece = get(7-i, j);
				builder.append(String.format("[%s]", (piece == null)  ? 
						"" : piece.toString()));
			}
			builder.append("\n");
		}
		
		return builder.toString();
	}
	
	public Object clone() {
		Chessboard<T> board = new Chessboard<T>();
		
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				T value = get(i, j);
				board.set(i, j, value == null ? null : (T) value.clone());
			}
		}
		
		return board;
	}
}
