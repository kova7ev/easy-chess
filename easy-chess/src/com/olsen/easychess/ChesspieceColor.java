package com.olsen.easychess;

public enum ChesspieceColor {
	White,
	Black,
	None;
	
	public static ChesspieceColor Parse(String responce) {
		if (responce.equals("W")){
			return White;
		}else if (responce.equals("B")){
			return Black;
		}
		
		return None;
	}
	
	public static ChesspieceColor Parse(int responce) {
		if (responce == 1){
			return White;
		}else if (responce == 0){
			return Black;
		}
		
		return None;
	}
}
