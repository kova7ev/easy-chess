package com.olsen.easychess;

public enum ChesspieceType {
	Rook, Knight, Bishop, King, Queen, Pawn, None;

	public static ChesspieceType Parse(int responce) {
		switch (responce) {
		case 1:
			return King;
		case 0:
			return Queen;
		case 2:
			return Rook;
		case 3:
			return Bishop;
		case 4:
			return Knight;
		case 5:
			return Pawn;
		default:
			//Typo in indexes file: instead 5 writen 55
			return Pawn;
		}
	}
}
