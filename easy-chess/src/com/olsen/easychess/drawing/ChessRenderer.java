/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.olsen.easychess.drawing;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.olsen.easychess.Chessboard;
import com.olsen.easychess.Chesspiece;
import com.olsen.easychess.R;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Provides drawing instructions for a GLSurfaceView object. This class must
 * override the OpenGL ES drawing lifecycle methods:
 * <ul>
 * <li>{@link android.opengl.GLSurfaceView.Renderer#onSurfaceCreated}</li>
 * <li>{@link android.opengl.GLSurfaceView.Renderer#onDrawFrame}</li>
 * <li>{@link android.opengl.GLSurfaceView.Renderer#onSurfaceChanged}</li>
 * </ul>
 */
public class ChessRenderer implements GLSurfaceView.Renderer {

	private static final String TAG = "MyGLRenderer";

	private static final String VERTEX_SHADER_CODE = ""
			+
			// This matrix member variable provides a hook to manipulate
			// the coordinates of the objects that use this vertex shader
			"uniform mat4 uMVPMatrix;" + "attribute vec4 aPosition;"
			+ "attribute vec2 aTexCoords;" + "varying vec2 vTexCoords;"
			+ "void main(){" + "  vTexCoords = aTexCoords;"
			+ "  gl_Position = uMVPMatrix * aPosition;" + "}";

	private static final String FRAGMENT_SHADER_CODE = ""
			+ "precision mediump float;"
			+ "uniform sampler2D uTexture;"
			+ "uniform float uAlpha;"
			+ "varying vec2 vTexCoords;"
			+ "void main(){"
			+ "  gl_FragColor = texture2D(uTexture, vTexCoords);"
			+ "  if (gl_FragColor.a == 1.0f) { gl_FragColor.a = uAlpha; } else { gl_FragColor.a = 0.0f; }"
			+ "}";

	// number of coordinates per vertex in this array
	private static final int COORDS_PER_VERTEX = 3;
	private static final int VERTEX_STRIDE_BYTES = COORDS_PER_VERTEX
			* GLUtil.BYTES_PER_FLOAT;

	// S, T (or X, Y)
	private static final int COORDS_PER_TEXTURE_VERTEX = 2;
	private static final int TEXTURE_VERTEX_STRIDE_BYTES = COORDS_PER_TEXTURE_VERTEX
			* GLUtil.BYTES_PER_FLOAT;

	private static final float[] SQUARE_TEXTURE_VERTICES = { 0, 0, // top left
			0, 1, // bottom left
			1, 1, // bottom right

			0, 0, // top left
			1, 1, // bottom right
			1, 0, // top right
	};

	private FloatBuffer mTextureCoordsBuffer;
	private FloatBuffer mVertexBuffer;

	private static int sProgramHandle;
	private static int sAttribPositionHandle;
	private static int sAttribTextureCoordsHandle;
	private static int sUniformAlphaHandle;
	private static int sUniformTextureHandle;
	private static int sUniformMVPMatrixHandle;

	// Model and view matrices. Projection and MVP stored in picture set
	private final float[] mMMatrix = new float[16];
	private final float[] mVMatrix = new float[16];
	private final float[] mPMatrix = new float[16];
	private final float[] mMVPMatrix = new float[16];

	private int mWidth;
	private int mHeight;
	private float mRatio;

	private int mIndexX;
	private int mIndexY;
	private boolean mNeedHighlightTile = false;

	private int mStartPosX;
	private int mStartPosY;

	private Bitmap mBlackTile;
	private Bitmap mWhiteTile;
	private Bitmap mHighlightTile;

	private int mTextureHandles[] = new int[6];
	private Map<Chesspiece, Integer> mChessmenTextures = new HashMap<Chesspiece, Integer>();

	private View mContext;
	private boolean mLock = false;
	
	public static Chessboard<Chesspiece> Board;
	public static List<Chessboard<Chesspiece>> mHistory;
	public static int mIndex;
	public static String PackageName;

	public ChessRenderer(View context) {
		mContext = context;
	}

	@Override
	public void onSurfaceCreated(GL10 unused, EGLConfig config) {
		GLES20.glEnable(GLES20.GL_BLEND);
		GLES20.glBlendFuncSeparate(GLES20.GL_SRC_ALPHA,
				GLES20.GL_ONE_MINUS_SRC_ALPHA, GLES20.GL_ONE, GLES20.GL_ONE);
		GLES20.glClearColor(172 / 255, 150 / 255, 150 / 255, 1.0f);

		// Set the camera position (View matrix)
		Matrix.setLookAtM(mVMatrix, 0, 0, 0, 1, 0, 0, -1, 0, 1, 0);
		initGl();
	}

	public void initGl() {
		// Initialize shaders and create/link program
		int vertexShaderHandle = GLUtil.loadShader(GLES20.GL_VERTEX_SHADER,
				VERTEX_SHADER_CODE);
		int fragShaderHandle = GLUtil.loadShader(GLES20.GL_FRAGMENT_SHADER,
				FRAGMENT_SHADER_CODE);

		sProgramHandle = GLUtil.createAndLinkProgram(vertexShaderHandle,
				fragShaderHandle, null);

		sAttribPositionHandle = GLES20.glGetAttribLocation(sProgramHandle,
				"aPosition");
		sAttribTextureCoordsHandle = GLES20.glGetAttribLocation(sProgramHandle,
				"aTexCoords");

		sUniformMVPMatrixHandle = GLES20.glGetUniformLocation(sProgramHandle,
				"uMVPMatrix");
		sUniformTextureHandle = GLES20.glGetUniformLocation(sProgramHandle,
				"uTexture");
		sUniformAlphaHandle = GLES20.glGetUniformLocation(sProgramHandle,
				"uAlpha");

		mTextureCoordsBuffer = GLUtil.asFloatBuffer(SQUARE_TEXTURE_VERTICES);

		for (int i = 0; i < 6; i++) {
			Bitmap bitmap = loadTile(i);
			mTextureHandles[i] = GLUtil.loadTexture(bitmap);
		}
		
		for (Chesspiece piece : Chesspiece.getAllPieces()) {
			Bitmap bitmap = loadCheesmenTexture(piece);
			if (bitmap != null) {
				mChessmenTextures.put(piece, GLUtil.loadTexture(bitmap));
			}
		}
	}
	
	public void releaseGL(){
		GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		GLUtil.checkGlError("glDeleteTextures");
		mTextureHandles = null;
		
		mTextureHandles = new int[mChessmenTextures.size()];
		int index = 0;
		for (Chesspiece piece : mChessmenTextures.keySet()){
			mTextureHandles[index++] = mChessmenTextures.get(piece);
		}
		GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		GLUtil.checkGlError("glDeleteTextures");
	}

	private void drawChessboard() {
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				int color = (i + j + 2) % 2;
				drawTile(i, j, color, 0, (1.0f - mRatio) / 2);
			}
		}
	}

	private void drawTile(int posY, int posX, int color, float offsetX,
			float offsetY) {
		float sideX = 1.0f / 10;
		float sideY = (1 * mRatio) / 10;

		float vertices[] = new float[] { posX * sideX + offsetX,
				(posY + 1) * sideY + offsetY, 0, // top left
				posX * sideX + offsetX, posY * sideY + offsetY, 0, // bottom
																	// left
				(posX + 1) * sideX + offsetX, posY * sideY + offsetY, 0, // bottom
																			// right

				posX * sideX + offsetX, (posY + 1) * sideY + offsetY, 0, // top
																			// left
				(posX + 1) * sideX + offsetX, posY * sideY + offsetY, 0, // bottom
																			// right
				(posX + 1) * sideX + offsetX, (posY + 1) * sideY + offsetY, 0, // top
																				// right
		};

		mVertexBuffer = GLUtil.asFloatBuffer(vertices);

		// Add program to OpenGL ES environment
		GLES20.glUseProgram(sProgramHandle);

		// Apply the projection and view transformation
		// Model matrix is ignored while
		Matrix.multiplyMM(mMVPMatrix, 0, mVMatrix, 0, mPMatrix, 0);

		GLES20.glUniformMatrix4fv(sUniformMVPMatrixHandle, 1, false, mMVPMatrix, 0);
		GLUtil.checkGlError("glUniformMatrix4fv");

		// Set up vertex buffer
		GLES20.glEnableVertexAttribArray(sAttribPositionHandle);
		GLES20.glVertexAttribPointer(sAttribPositionHandle, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, VERTEX_STRIDE_BYTES, mVertexBuffer);

		// Set up texture stuff
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glUniform1i(sUniformTextureHandle, 0);

		GLES20.glEnableVertexAttribArray(sAttribTextureCoordsHandle);
		GLES20.glVertexAttribPointer(sAttribTextureCoordsHandle,
				COORDS_PER_TEXTURE_VERTEX, GLES20.GL_FLOAT, false,
				TEXTURE_VERTEX_STRIDE_BYTES, mTextureCoordsBuffer);

		// Set the alpha
		GLES20.glUniform1f(sUniformAlphaHandle, 1.0f);

		// TODO: Load textures

		// Bitmap bitmap = loadTile(color);
		// mTextureHandles = new int[1];
		// mTextureHandles[0] = GLUtil.loadTexture(bitmap);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandles[color]);
		GLUtil.checkGlError("glBindTexture");

		// Draw the two triangles
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length
				/ COORDS_PER_VERTEX);

		GLES20.glDisableVertexAttribArray(sAttribPositionHandle);
		GLES20.glDisableVertexAttribArray(sAttribTextureCoordsHandle);
	}
	
	private void drawNaigation(float posX, float posY, float side, int textureHandler) {
		float vertices[] = new float[] { posX - side / 2, posY + side * mRatio/ 2, 0,// top left
										 posX - side / 2, posY - side * mRatio/ 2, 0,// bottom left
										 posX + side / 2, posY - side * mRatio/ 2, 0,// bottom right

										 posX - side / 2, posY + side * mRatio/ 2, 0,// top left
										 posX + side / 2, posY - side * mRatio/ 2, 0,// bottom right
										 posX + side / 2, posY + side * mRatio/ 2, 0,// top right
		};
	
				mVertexBuffer = GLUtil.asFloatBuffer(vertices);

				// Add program to OpenGL ES environment
				GLES20.glUseProgram(sProgramHandle);

				// Apply the projection and view transformation
				// Model matrix is ignored while
				Matrix.multiplyMM(mMVPMatrix, 0, mVMatrix, 0, mPMatrix, 0);

				GLES20.glUniformMatrix4fv(sUniformMVPMatrixHandle, 1, false,
						mMVPMatrix, 0);
				GLUtil.checkGlError("glUniformMatrix4fv");

				// Set up vertex buffer
				GLES20.glEnableVertexAttribArray(sAttribPositionHandle);
				GLES20.glVertexAttribPointer(sAttribPositionHandle, COORDS_PER_VERTEX,
						GLES20.GL_FLOAT, false, VERTEX_STRIDE_BYTES, mVertexBuffer);

				// Set up texture stuff
				GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
				GLES20.glUniform1i(sUniformTextureHandle, 0);

				GLES20.glEnableVertexAttribArray(sAttribTextureCoordsHandle);
				GLES20.glVertexAttribPointer(sAttribTextureCoordsHandle,
						COORDS_PER_TEXTURE_VERTEX, GLES20.GL_FLOAT, false,
						TEXTURE_VERTEX_STRIDE_BYTES, mTextureCoordsBuffer);

				// Set the alpha
				GLES20.glUniform1f(sUniformAlphaHandle, 1.0f);

				// TODO: Load textures

				// Bitmap bitmap = loadBackground();
				// mTextureHandles = new int[1];
				// mTextureHandles[0] = GLUtil.loadTexture(bitmap);
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandles[textureHandler]);
				GLUtil.checkGlError("glBindTexture");

				// Draw the two triangles
				GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length
						/ COORDS_PER_VERTEX);

				GLES20.glDisableVertexAttribArray(sAttribPositionHandle);
				GLES20.glDisableVertexAttribArray(sAttribTextureCoordsHandle);
				// GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
				// GLUtil.checkGlError("glDeleteTextures");
				// mTextureHandles = null;
	}


	private void drawChessmen(int posX, int posY, int textureHandler,
			float offsetX, float offsetY) {
		float sideX = 1.0f / 10;
		float sideY = (1 * mRatio) / 10;

		float vertices[] = new float[] { posX * sideX + offsetX,
				(posY + 1) * sideY + offsetY, 0, // top left
				posX * sideX + offsetX, posY * sideY + offsetY, 0, // bottom
																	// left
				(posX + 1) * sideX + offsetX, posY * sideY + offsetY, 0, // bottom
																			// right

				posX * sideX + offsetX, (posY + 1) * sideY + offsetY, 0, // top
																			// left
				(posX + 1) * sideX + offsetX, posY * sideY + offsetY, 0, // bottom
																			// right
				(posX + 1) * sideX + offsetX, (posY + 1) * sideY + offsetY, 0, // top
																				// right
		};

		mVertexBuffer = GLUtil.asFloatBuffer(vertices);

		// Add program to OpenGL ES environment
		GLES20.glUseProgram(sProgramHandle);

		// Apply the projection and view transformation
		// Model matrix is ignored while
		Matrix.multiplyMM(mMVPMatrix, 0, mVMatrix, 0, mPMatrix, 0);

		GLES20.glUniformMatrix4fv(sUniformMVPMatrixHandle, 1, false,
				mMVPMatrix, 0);
		GLUtil.checkGlError("glUniformMatrix4fv");

		// Set up vertex buffer
		GLES20.glEnableVertexAttribArray(sAttribPositionHandle);
		GLES20.glVertexAttribPointer(sAttribPositionHandle, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, VERTEX_STRIDE_BYTES, mVertexBuffer);

		// Set up texture stuff
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glUniform1i(sUniformTextureHandle, 0);

		GLES20.glEnableVertexAttribArray(sAttribTextureCoordsHandle);
		GLES20.glVertexAttribPointer(sAttribTextureCoordsHandle,
				COORDS_PER_TEXTURE_VERTEX, GLES20.GL_FLOAT, false,
				TEXTURE_VERTEX_STRIDE_BYTES, mTextureCoordsBuffer);

		// Set the alpha
		GLES20.glUniform1f(sUniformAlphaHandle, 1.0f);

		// TODO: Load textures

		// Bitmap bitmap = loadTile(color);
		// mTextureHandles = new int[1];
		// mTextureHandles[0] = GLUtil.loadTexture(bitmap);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandler);
		GLUtil.checkGlError("glBindTexture");

		// Draw the two triangles
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length
				/ COORDS_PER_VERTEX);

		GLES20.glDisableVertexAttribArray(sAttribPositionHandle);
		GLES20.glDisableVertexAttribArray(sAttribTextureCoordsHandle);
		// GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		// GLUtil.checkGlError("glDeleteTextures");
		// mTextureHandles = null;
	}

	public void drawBackground() {
		// calaulate vertices
		float vertices[] = new float[] { 0, 1, 0, // top left
				0, 0, 0, // bottom left
				1, 0, 0, // bottom right

				0, 1, 0, // top left
				1, 0, 0, // bottom right
				1, 1, 0, // top right
		};

		mVertexBuffer = GLUtil.asFloatBuffer(vertices);

		// Add program to OpenGL ES environment
		GLES20.glUseProgram(sProgramHandle);

		// Apply the projection and view transformation
		// Model matrix is ignored while
		Matrix.multiplyMM(mMVPMatrix, 0, mVMatrix, 0, mPMatrix, 0);

		GLES20.glUniformMatrix4fv(sUniformMVPMatrixHandle, 1, false,
				mMVPMatrix, 0);
		GLUtil.checkGlError("glUniformMatrix4fv");

		// Set up vertex buffer
		GLES20.glEnableVertexAttribArray(sAttribPositionHandle);
		GLES20.glVertexAttribPointer(sAttribPositionHandle, COORDS_PER_VERTEX,
				GLES20.GL_FLOAT, false, VERTEX_STRIDE_BYTES, mVertexBuffer);

		// Set up texture stuff
		GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		GLES20.glUniform1i(sUniformTextureHandle, 0);

		GLES20.glEnableVertexAttribArray(sAttribTextureCoordsHandle);
		GLES20.glVertexAttribPointer(sAttribTextureCoordsHandle,
				COORDS_PER_TEXTURE_VERTEX, GLES20.GL_FLOAT, false,
				TEXTURE_VERTEX_STRIDE_BYTES, mTextureCoordsBuffer);

		// Set the alpha
		GLES20.glUniform1f(sUniformAlphaHandle, 1.0f);

		// TODO: Load textures

		// Bitmap bitmap = loadBackground();
		// mTextureHandles = new int[1];
		// mTextureHandles[0] = GLUtil.loadTexture(bitmap);
		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandles[3]);
		GLUtil.checkGlError("glBindTexture");

		// Draw the two triangles
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, vertices.length
				/ COORDS_PER_VERTEX);

		GLES20.glDisableVertexAttribArray(sAttribPositionHandle);
		GLES20.glDisableVertexAttribArray(sAttribTextureCoordsHandle);
		// GLES20.glDeleteTextures(mTextureHandles.length, mTextureHandles, 0);
		// GLUtil.checkGlError("glDeleteTextures");
		// mTextureHandles = null;
	}

	private Bitmap loadTile(int color) {

		Drawable myDrawable = null;

		if (color == 0) {
			if (mWhiteTile == null) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.tile_white);
				mWhiteTile = ((BitmapDrawable) myDrawable).getBitmap();
			}
			return mWhiteTile;
		} else if (color == 1) {
			if (mBlackTile == null) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.tile_black);
				mBlackTile = ((BitmapDrawable) myDrawable).getBitmap();
			}
			return mBlackTile;
		} else if (color == 2) {
			if (mHighlightTile == null) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.tile_highlight);
				mHighlightTile = ((BitmapDrawable) myDrawable).getBitmap();
			}
			return mHighlightTile;
		} else if (color == 3) {
			myDrawable = mContext.getResources().getDrawable(
					R.drawable.background);
			return ((BitmapDrawable) myDrawable).getBitmap();
		}else if (color == 4) {
			myDrawable = mContext.getResources().getDrawable(
					R.drawable.prev);
			return ((BitmapDrawable) myDrawable).getBitmap();
		}
		else if (color == 5) {
			myDrawable = mContext.getResources().getDrawable(
					R.drawable.next);
			return ((BitmapDrawable) myDrawable).getBitmap();
		}

		return null;
	}

	private Bitmap loadCheesmenTexture(Chesspiece piece) {
		String name = String.format("%s_%s", piece.getType().name().toLowerCase(), piece.getColor().name().toLowerCase());
		int id = mContext.getResources().getIdentifier(name, "drawable", PackageName);

		BitmapDrawable drawable = (BitmapDrawable) mContext.getResources().getDrawable(id);
		return drawable.getBitmap();
	}
	
	@Override
	public void onDrawFrame(GL10 unused) {
		if (!mLock){
			drawBackground();
			drawChessboard();
			drawHighlightTile();
			drawChessmens();
			drawNavigationBottons();
		}
	}

	private void drawNavigationBottons() {
		float margin = (1.0f * mHeight - 8.0f * mWidth / 10) / mHeight;
		
		float prev_x = 1.0f / 4;
		float next_x = 3.0f / 4;
		float y = margin / 4;
		
		drawNaigation(prev_x , y, margin/2.7f, 4);
		drawNaigation(next_x , y, margin/2.7f, 5);
	}

	private void drawChessmens() {
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				
				if (mLock){
					return;
				}
				
				Chesspiece piece = mHistory.get(mIndex).get(i, j);
				if (piece != null) {
					drawChessmen(j + 1, i + 1, mChessmenTextures.get(piece), 0, (1.0f - mRatio) / 2);
				}
			}
		}
	}

	private void drawHighlightTile() {
		if (mIndexX < 0 || mIndexX > 7 || mIndexY < 0 || mIndexY > 7) {
			return;
		}

		if (mNeedHighlightTile) {
			drawTile(mIndexX+1, mIndexY+1, 2, 0, (1.0f - mRatio) / 2);
		} else {
			int color = (mIndexX + mIndexY + 2) % 2;
			drawTile(mIndexX+1, mIndexY+1, color, 0, (1.0f - mRatio) / 2);
		}
	}

	@Override
	public void onSurfaceChanged(GL10 unused, int width, int height) {
		// Adjust the viewport based on geometry changes,
		// such as screen rotation
		GLES20.glViewport(0, 0, width, height);

		mWidth = width;
		mHeight = height;
		mRatio = (float) width / height;

		// this projection matrix is applied to object coordinates
		// in the onDrawFrame() method
		Matrix.orthoM(mPMatrix, 0, 0, 1, 0, 1, -1, 1);
	}
	private double distance(float x0, float y0, float x1, float y1){
		return Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2));
	}

	public void onTouch(MotionEvent e) {
		float x = e.getX();
		float y = e.getY();

		float normX = x / mWidth;
		float normY = (1 - y / mHeight);

		mIndexY = (int) (normX * 10) - 1;
		mIndexX = (int) ((normY - (1.0f - mRatio) / 2.0f) / (mRatio / 10)) - 1;
		
		if (e.getAction() == MotionEvent.ACTION_DOWN) {
			mNeedHighlightTile = true;
			mStartPosX = mIndexX;
			mStartPosY = mIndexY;
			
			//start nav button handler
			Log.w(TAG, String.format("x,y=<%f,%f>", x, y));
			float margin = (1.0f * mHeight - 8.0f * mWidth / 10) / mHeight;
			
			float nav_prev_x = 1.0f / 4 * mWidth;
			float nav_next_x = 3.0f / 4 * mWidth;
			float nav_y = (margin / 4) * mHeight;
			
			float side = (margin / 2.7f) * mHeight;
			
			//Log.w(TAG, String.format("prev x,y=<%f,%f>", nav_prev_x, nav_y));
			//Log.w(TAG, String.format("next x,y=<%f,%f>", nav_next_x, nav_y));
			//Log.w(TAG, String.format("side=%f", side));
			
			if (distance(nav_prev_x, nav_y, x, mHeight - y) < side/2){
				Log.w(TAG, "nav prev");
				
				if (mIndex > 0){
					mIndex--;
					Board = mHistory.get(mIndex);
				}
			}
			
			if (distance(nav_next_x, nav_y, x, mHeight - y) < side/2){
				Log.w(TAG, "nav next");
				
				if (mIndex < (mHistory.size()-1)){
					mIndex++;
					Board = mHistory.get(mIndex);
				}
			}
			//end nav button handler
		} else if (e.getAction() == MotionEvent.ACTION_UP) {
			mNeedHighlightTile = false;
			
			if (mIndexX != mStartPosX || mIndexY != mStartPosY){
				Chesspiece piece = Board.get(mStartPosX, mStartPosY);
				
				if (piece != null){
					mLock = true;
					
					if (mIndex != (mHistory.size()-1)){						
						int size = mHistory.size();
						while (++mIndex < size){
							mHistory.remove(mHistory.size()-1);
						}
						
						mIndex = mHistory.size()-1;
						Board = (Chessboard<Chesspiece>) mHistory.get(mIndex).clone();
						//mHistory.add((Chessboard<Chesspiece>) Board.clone());
					}
					
					Board.set(mIndexX, mIndexY, piece);
					Board.set(mStartPosX, mStartPosY, null);
					mHistory.add((Chessboard<Chesspiece>) Board.clone());
					mIndex = mHistory.size()-1;
					
					mLock = false;
				}
			}
		}
	}
}