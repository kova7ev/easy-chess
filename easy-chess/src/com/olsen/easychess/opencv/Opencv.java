package com.olsen.easychess.opencv;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.ml.CvNormalBayesClassifier;

import com.olsen.easychess.Chessboard;
import com.olsen.easychess.Chesspiece;
import com.olsen.easychess.ChesspieceColor;
import com.olsen.easychess.ChesspieceType;
import com.olsen.easychess.drawing.ChessRenderer;
import com.olsen.easychess.utils.FileIOHelper;

import android.app.Activity;
import android.util.Log;

public class Opencv {
private static String TAG = "OpencvHelper";
	
	private static boolean mInitialized = true;
	private static CvNormalBayesClassifier mClassifierPiece2;
	private static CvNormalBayesClassifier mClassifierPiece1;
	private static CvNormalBayesClassifier mClassifier1Color;
	private static Activity mContext;
	
	public static void init(Activity context){
		mContext = context;
		ChessRenderer.PackageName = context.getPackageName();
	}
	
	public static void initClassifier2(File file){
		long time = Calendar.getInstance().getTimeInMillis();
		mClassifierPiece2 = loadClassifierFromFile(file.getAbsolutePath());
		//mInitialized = true;
		long time_now = Calendar.getInstance().getTimeInMillis();
		Log.w(TAG, "'mClassifierPiece2' loaded (" + (time_now - time)/1000 + "." + (time_now - time)%1000 + ")");
	}
	
	public static void initClassifier1(File file){
		long time = Calendar.getInstance().getTimeInMillis();
		mClassifierPiece1 = loadClassifierFromFile(file.getAbsolutePath());
		//mInitialized = true;
		long time_now = Calendar.getInstance().getTimeInMillis();
		Log.w(TAG, "'mClassifierPiece1' loaded (" + (time_now - time)/1000 + "." + (time_now - time)%1000 + ")");
	}
	
	public static void initClassifier1Color(File file){
		long time = Calendar.getInstance().getTimeInMillis();
		mClassifier1Color = loadClassifierFromFile(file.getAbsolutePath());
		//mInitialized = true;
		long time_now = Calendar.getInstance().getTimeInMillis();
		Log.w(TAG, "'mClassifier1Color' loaded (" + (time_now - time)/1000 + "." + (time_now - time)%1000 + ")");
	}
	
	private static CvNormalBayesClassifier loadClassifierFromFile(String path) {
		CvNormalBayesClassifier classifier = new CvNormalBayesClassifier();
		classifier.load(path);
		return classifier;
	}
	
	public static Chessboard<Chesspiece> recognize(Mat image){
		if (!mInitialized){
			Log.w(TAG, "OpenCV not initialized. First call OpencvHelper.init(<path-to-classifier-file>)");
		}
		
		Chessboard<Chesspiece> pieces = null;//process(image);
		
		if (pieces == null){
			return null;
		}
		
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				Chesspiece piece = pieces.get(i, j);
				if (piece != null){
					//int responce = predict(mClassifierPiece1, piece.getMat());
					//piece.setType(ChesspieceType.Parse(responce));
				}
			}
		}
		
		return pieces;
	}

	
	public void train(String path) {
		Map<String, Integer> indexes = loadIndexes(path);
		Map<String, Mat> images = loadImages(indexes, path);

		String index_file = String.format("%s/indexes", path);

		// prepare feature vectors

		float train_set_part = 0.1f;
		int end_training_set = (int) (images.size() - images.size() * train_set_part);

		Mat traning_set = new Mat(end_training_set, 20 * 20, CvType.CV_32FC1);
		Mat traning_responce = new Mat(end_training_set, 1, CvType.CV_32FC1);
		Mat data_set = new Mat(images.size() - end_training_set, 20 * 20, CvType.CV_32FC1);
		Mat data_responce = new Mat(images.size() - end_training_set, 1, CvType.CV_32FC1);

		List<String> traning_set_indexes = new ArrayList<String>();

		prepareFeatureVectors(images, indexes, traning_set, traning_responce, data_set, data_responce, traning_set_indexes, end_training_set);
		writeTrainSetIndexes(path, traning_set_indexes);
		CvNormalBayesClassifier classifier = train(path, traning_set, traning_responce);
		writeClassifierToFile(classifier, path);
	}
	
	public void validate(String path) {
		Map<String, Integer> indexes = loadIndexes(path);
		List<Integer> validation_indexes = loadValidationIndexes(path);
		Map<String, Mat> images = loadImages(indexes, path);

		// prepare feature vectors

		int size = indexes.size() - validation_indexes.size();
		
		Mat data_set = new Mat(size, 20 * 20, CvType.CV_32FC1);
		Mat data_responce = new Mat(size, 1, CvType.CV_32FC1);

		prepareFeatureVectors(images, indexes, validation_indexes, data_set, data_responce);

		//writeTrainSetIndexes(path, traning_set_indexes);
		//CvNormalBayesClassifier classifier = train(path, traning_set, traning_responce);
		//writeClassifierToFile(classifier, path);

		CvNormalBayesClassifier classifier = loadClassifierFromFile(path);

		Mat responces = new Mat();
		classifier.predict(data_set, responces);
		
		int count = 0;
		for (int i = 0; i < responces.size().height; i++) {
			
			if (data_responce.get(i, 0)[0] == responces.get(i, 0)[0]) {
				System.out.println(String.format("predict:%f(%f)", responces.get(i, 0)[0], data_responce.get(i, 0)[0]));
				count++;
			}else{
				System.err.println(String.format("predict:%f(%f)", responces.get(i, 0)[0], data_responce.get(i, 0)[0]));
			}

		}

		System.out.println();
		System.out.println(String.format("Tested : %f", data_responce.size().height));
		System.out.println(String.format("Right predicted: %f", 1.0f * count / data_responce.size().height));
	}

	
	public void check(String path){
		Mat image = Highgui.imread(path);

		//resize
		float scale = calculateScale(image);
		Imgproc.resize(image, image, new Size(0, 0), scale, scale, Imgproc.INTER_CUBIC);

		//localize board
		Mat morphology = morph(image, Imgproc.MORPH_OPEN, Imgproc.MORPH_RECT, 5);
		Mat edges = edges(morphology);
		Mat mask = image.clone();
		MatOfPoint external_contour = board(edges, mask);
		Mat board = mask.mul(image);
		Mat rect_board = new Mat(board.size(), CvType.CV_8U);
		warpPerspective(board, rect_board, external_contour);
		image = submat(rect_board, external_contour);
	}
	
	public static Chessboard<Chesspiece> process1(Mat image){
		if (mClassifierPiece1 == null){
			Opencv.initClassifier1(FileIOHelper.open(mContext, "classifier/classifier1.rules"));
		}
		
		if (mClassifier1Color == null){
			Opencv.initClassifier1Color(FileIOHelper.open(mContext, "classifier/classifier1-color.rules"));
		}
		
		//pieces
		Map<String, Mat> colors = new HashMap<String, Mat>();
		Map<String, Mat> pieces = new HashMap<String, Mat>();
		
		recognizeChessPieces2(image, colors, pieces);
		
		//Map<String, String> game = new HashMap<String, String>();
		//CvNormalBayesClassifier classifierPiece = loadClassifierFromFile("/root/Repository/Java/opencv/samples/training-set-3");
		//CvNormalBayesClassifier classifierColor = loadClassifierFromFile("/root/Repository/Java/opencv/samples/training-set-color-3");

		Chessboard<Chesspiece> chessboard = new Chessboard<Chesspiece>();
		
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", i, j);
				if (pieces.containsKey(key)) {
					Mat piece = pieces.get(key);
					Mat color = colors.get(key);
					int responce1 = predict(mClassifierPiece1, piece);
					int responce2 = predict(mClassifier1Color, color);
					
					Chesspiece p = new Chesspiece();
					p.setType(ChesspieceType.Parse(responce1));
					p.setColor(ChesspieceColor.Parse(responce2));
					chessboard.set(i-1, j-1, p);
				}
			}
		}

		for (int i = 1; i < 9; i++) {
			StringBuilder builder = new StringBuilder();
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", 9 - i, j);
				Chesspiece piece = chessboard.get(i-1, j-1);
				
				if (piece == null){
					builder.append("[]");
				}else{
					builder.append(String.format("[%s_%s]", piece.getType().name(), piece.getColor().name()));
				}
			}
			Log.e(TAG, builder.toString());
		}
		
		return chessboard;

	}
	
	public static Chessboard<Chesspiece> process(Mat image){
			//resize
			float scale = calculateScale(image);
			Imgproc.resize(image, image, new Size(0, 0), scale, scale, Imgproc.INTER_CUBIC);

			//localize board
			Mat morphology = morph(image, Imgproc.MORPH_OPEN, Imgproc.MORPH_RECT, 5);
			Mat edges = edges(morphology);
			Mat mask = image.clone();
			MatOfPoint external_contour = board(edges, mask);
			
			if (external_contour == null){
				return null;
			}
			
			Mat board = mask.mul(image);
			Mat rect_board = new Mat(board.size(), CvType.CV_8U);
			warpPerspective(board, rect_board, external_contour);
			image = submat(rect_board, external_contour);
			
			List<Integer> peaks = hist(image);
			Log.w(TAG, String.format("Number finded peaks: %d", peaks.size()));
			
			if (peaks.size() == 2){
				return process2(image);
			}else{
				return process1(image);
			}
	}

	
	public static Chessboard<Chesspiece> process2(Mat image){
		if (mClassifierPiece2 == null){
			Opencv.initClassifier2(FileIOHelper.open(mContext, "classifier/classifier2.rules"));
		}
		
		//pieces
		Map<String, String> colors = new HashMap<String, String>();
		Map<String, Mat> pieces = new HashMap<String, Mat>();
		
		recognizeChessPieces(image, colors, pieces);
		
		//Map<String, String> game = new HashMap<String, String>();
		//CvNormalBayesClassifier classifier = loadClassifierFromFile("/root/Repository/Java/opencv/samples/training-set-2");
		Chessboard<Chesspiece> chessboard = new Chessboard<Chesspiece>();
		
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", i, j);
				
				if (pieces.containsKey(key)) {
					Mat piece = pieces.get(key);
					int responce = predict(mClassifierPiece2, piece);
					
					Chesspiece p = new Chesspiece();
					p.setType(ChesspieceType.Parse(responce));
					p.setColor(ChesspieceColor.Parse(colors.get(key)));
					chessboard.set(i-1, j-1, p);
				}
			}
		}
		
		for (int i = 1; i < 9; i++) {
			StringBuilder builder = new StringBuilder();
			for (int j = 1; j < 9; j++) {
				Chesspiece piece = chessboard.get(i-1, j-1);
				if (piece == null){
					builder.append("[]");
				}else{
					builder.append(String.format("[%s_%s]", piece.getType().name(), piece.getColor().name()));
				}
			}
			Log.e(TAG, builder.toString());
		}
		
		return chessboard;
	}

	public  void main(String[] args) {
		//Mat image = Highgui.imread("/root/Downloads/DSC_0093.JPG");
		Mat image = Highgui.imread("/root/Repository/Java/opencv/samples/samples-2/DSC_0090.JPG");

		//resize
		float scale = calculateScale(image);
		Imgproc.resize(image, image, new Size(0, 0), scale, scale, Imgproc.INTER_CUBIC);

		//localize board
		Mat morphology = morph(image, Imgproc.MORPH_OPEN, Imgproc.MORPH_RECT, 5);
		Mat edges = edges(morphology);
		Mat mask = image.clone();
		MatOfPoint external_contour = board(edges, mask);
		Mat board = mask.mul(image);
		Mat rect_board = new Mat(board.size(), CvType.CV_8U);
		warpPerspective(board, rect_board, external_contour);
		image = submat(rect_board, external_contour);
		
		//pieces
		Map<String, String> colors = new HashMap<String, String>();
		Map<String, Mat> pieces = new HashMap<String, Mat>();
		
		recognizeChessPieces(image, colors, pieces);
		
		Map<String, String> game = new HashMap<String, String>();
		CvNormalBayesClassifier classifier = loadClassifierFromFile("/root/Repository/Android/easy-chess/easy-chess/assets/classifier");

		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", i, j);
				if (pieces.containsKey(key)) {
					Mat piece = pieces.get(key);
					int responce = predict(classifier, piece);
					game.put(key, String.format("[%s_%s]",
							responceToValue(responce), colors.get(key)));
				} else {
					game.put(key, "[     ]");
				}
			}
		}

		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", 9 - i, j);
				System.out.print(game.get(key));
			}
			System.out.println();
		}
	}

	private  String responceToValue(int res) {
		switch (res) {
		case 1:
			return "KNG";
		case 0:
			return "QEN";
		case 2:
			return "ROK";
		case 3:
			return "BIS";
		case 4:
			return "KNT";
		case 5:
			return "PWN";
		default:
			return "?";
		}
	}

	private static  int predict(CvNormalBayesClassifier classifier, Mat piece) {
		Mat data_set = new Mat(1, 20 * 20, CvType.CV_32FC1);
		Imgproc.resize(piece, piece, new Size(20, 20));
		Core.multiply(piece, new Scalar(1.0f / 255), piece);
		
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 20; j++) {
				data_set.put(0, i * 20 + j, piece.get(i, j)[0]);
			}
		}

		return (int) classifier.predict(data_set);
	}

	public void prepareTrainingSet(String base_dir, String in, String out, 
			Integer name_index) {
		long time_total = Calendar.getInstance().getTimeInMillis();
		
		for (File file : new File(String.format("%s/%s", base_dir, in)).listFiles()) {
			if (file.getAbsolutePath().endsWith(".JPG")) {
				
				System.err.println(file.getAbsolutePath());
				
				String name = file.getName();
				int index = name.lastIndexOf(".");
				String base_name = name.substring(0, index);

				String dir = String.format("%s/%s", base_dir, out);
				File folder = new File(dir);
				if (!folder.exists()){
					folder.mkdir();
				}

				long time = Calendar.getInstance().getTimeInMillis();
				Mat image = Highgui.imread(file.getAbsolutePath());
				
				//resize
				float scale = calculateScale(image);
				Imgproc.resize(image, image, new Size(0, 0), scale, scale, Imgproc.INTER_CUBIC);

				//localize board
				Mat morphology = morph(image, Imgproc.MORPH_OPEN, Imgproc.MORPH_RECT, 5);
				Mat edges = edges(morphology);
				Mat mask = image.clone();
				MatOfPoint external_contour = board(edges, mask);
				Mat board = mask.mul(image);
				Mat rect_board = new Mat(board.size(), CvType.CV_8U);
				warpPerspective(board, rect_board, external_contour);
				image = submat(rect_board, external_contour);
				
				name_index = recognizeChessPieces2(image, dir, name_index);
				
				//System.err.println(name_index);

				long time_current = Calendar.getInstance().getTimeInMillis();
				System.out.println((time_current - time) / 1000 + "." + (time_current - time) % 1000);
			}
		}

		long time_current = Calendar.getInstance().getTimeInMillis();
		System.out.println("TOTAL: " + (time_current - time_total) / 1000 + "." + (time_current - time_total) % 1000);

	}

	private Integer recognizeChessPieces(Mat image, String dir, Integer name_index) {
		List<Mat> split_pieces = recognizeCells2(image);
		
		Map<String, Mat> cleaned_pieces = new HashMap<String, Mat>();
		Map<String, MatOfPoint> pieces_contour = new HashMap<String, MatOfPoint>();

		for (int i = 0; i < split_pieces.size(); i++) {
			Mat piece = split_pieces.get(i);

			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			Mat hierarchy = new Mat();
			Imgproc.findContours(piece, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

			Mat drawing = Mat.zeros(piece.size(), CvType.CV_8U);

			List<MatOfPoint> filtred_contours = new ArrayList<MatOfPoint>();
			for (int j = 0; j < contours.size(); j++) {
				MatOfPoint contour = contours.get(j);
				float area = (float) Imgproc.contourArea(contour);

				if (area > 200) {
					filtred_contours.add(contour);
				}
			}

			for (int j = 0; j < filtred_contours.size(); j++) {
				//MatOfPoint contour = contours.get(j);
				// Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
				//Scalar color = new Scalar(255, 255, 255);
				Scalar color = new Scalar(1);
				Imgproc.drawContours(drawing, filtred_contours, j, color, Core.FILLED);
			}

			if (filtred_contours.size() > 0) {
				cleaned_pieces.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1), drawing);
				pieces_contour.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1),filtred_contours.get(0));
				//Highgui.imwrite(String.format("/root/debug/cell_%dx%s.png",i / 8 + 1, i % 8 + 1), drawing);
			}
		}

		image = bw(image, 200);
		//imshow(image, "binary");
		
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", i, j);
				if (cleaned_pieces.containsKey(key)) {
					Mat piece = new Mat(image.size(), image.type());
					Mat mask = cleaned_pieces.get(key);
					Core.multiply(image, mask, piece);

					MatOfPoint contour = pieces_contour.get(key);
					Moments moments = Imgproc.moments(contour);
					float cx = (float) (moments.get_m10() / moments.get_m00());
					float cy = (float) (moments.get_m01() / moments.get_m00());

					MatOfInt hull = new MatOfInt();
					Imgproc.convexHull(contour, hull);

					// MatOfPoint hull_curve = new MatOfPoint();
					// hull_curve.create((int) hull.size().height, 1,
					// CvType.CV_32SC2);
					float left = Float.MAX_VALUE;
					float right = Float.MIN_VALUE;
					float top = Float.MIN_VALUE;
					float botton = Float.MAX_VALUE;

					for (int k = 0; k < hull.size().height; k++) {
						int index = (int) hull.get(k, 0)[0];
						double[] point = new double[] { contour.get(index, 0)[0], contour.get(index, 0)[1] };

						left = (float) Math.min(left, point[0]);
						right = (float) Math.max(right, point[0]);
						top = (float) Math.max(top, point[1]);
						botton = (float) Math.min(botton, point[1]);
					}

					Mat p1 = bw2(image.submat(new Rect((int) cx - 25,(int) cy - 25, 50, 50)), 200);
					Mat p2 = mask.submat(new Rect((int) cx - 25, (int) cy - 25, 50, 50));
					Mat p3 = new Mat();
					Core.multiply(p1, p2, p3);
					//Mat p4 = p3;//bw(p3, 200);
					Mat p5 = new Mat();
					Core.multiply(p2, new Scalar(255), p5);

					// TODO: Use later for detect piece color
					
					int countBlack = 0;
					int countWhite = 0;
					for (int row = 0; row < p3.size().height; row++) {
						for (int col = 0; col < p3.size().width; col++) {
							if (p2.get(row, col)[0] == 1) {
								if (p3.get(row, col)[0] == 255) {
									countWhite++;
								} else {
									countBlack++;
								}
							}
						}
					}

					//String pos = String.format("%d%d", i, j);
					//out_color.put(key, (1.0f * countWhite / countBlack < 1.2f ? "B" : "W"));
					//out_piece.put(key, p5);

					// System.out.println(String.format("%dx%d -> %f", i, j,
					// 1.0f*countWhite/countBlacl));

					// p4 = bw(p3, 200);
					// Core.add(p4, new Scalar(255), p4);

					// Mat p5 = new Mat(p4.size(), p4.type());
					// Core.multiply(p4, p2, p5);
					// System.out.println(p3.dump());

					// System.out.println(rect);

					// float side = (float)
					// Math.sqrt(Imgproc.contourArea(contour));
					//Highgui.imwrite(String.format("%s/cell_%d%d_%s.png", dir, i, j, (1.0f*countWhite/countBlack < 1.2f ? "black" : "white")), p5);
					// System.out.println(String.format("%s/%d.png", dir,
					// name_index++));

					Highgui.imwrite(String.format("%s/%d.png", dir, name_index++), p5);
					System.err.println(Integer.toString(name_index));
				}
			}
		}
		
		return name_index;
	}

	private static  void recognizeChessPieces(Mat image, Map<String, String> out_color, Map<String, Mat> out_piece) {
		List<Mat> split_pieces = recognizeCells(image);
		
		Map<String, Mat> cleaned_pieces = new HashMap<String, Mat>();
		Map<String, MatOfPoint> pieces_contour = new HashMap<String, MatOfPoint>();

		for (int i = 0; i < split_pieces.size(); i++) {
			Mat piece = split_pieces.get(i);

			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			Mat hierarchy = new Mat();
			Imgproc.findContours(piece, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

			Mat drawing = Mat.zeros(piece.size(), CvType.CV_8U);

			List<MatOfPoint> filtred_contours = new ArrayList<MatOfPoint>();
			for (int j = 0; j < contours.size(); j++) {
				MatOfPoint contour = contours.get(j);
				float area = (float) Imgproc.contourArea(contour);

				if (area > 200) {
					filtred_contours.add(contour);
				}
			}

			for (int j = 0; j < filtred_contours.size(); j++) {
				//MatOfPoint contour = contours.get(j);
				// Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
				//Scalar color = new Scalar(255, 255, 255);
				Scalar color = new Scalar(1);
				Imgproc.drawContours(drawing, filtred_contours, j, color, Core.FILLED);
			}

			if (filtred_contours.size() > 0) {
				cleaned_pieces.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1), drawing);
				pieces_contour.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1),filtred_contours.get(0));
				//Highgui.imwrite(String.format("/root/debug/cell_%dx%s.png",i / 8 + 1, i % 8 + 1), drawing);
			}
		}

		image = bw(image, 200);
		//imshow(image, "binary");
		
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", i, j);
				if (cleaned_pieces.containsKey(key)) {
					Mat piece = new Mat(image.size(), image.type());
					Mat mask = cleaned_pieces.get(key);
					Core.multiply(image, mask, piece);

					MatOfPoint contour = pieces_contour.get(key);
					Moments moments = Imgproc.moments(contour);
					float cx = (float) (moments.get_m10() / moments.get_m00());
					float cy = (float) (moments.get_m01() / moments.get_m00());

					MatOfInt hull = new MatOfInt();
					Imgproc.convexHull(contour, hull);

					// MatOfPoint hull_curve = new MatOfPoint();
					// hull_curve.create((int) hull.size().height, 1,
					// CvType.CV_32SC2);
					float left = Float.MAX_VALUE;
					float right = Float.MIN_VALUE;
					float top = Float.MIN_VALUE;
					float botton = Float.MAX_VALUE;

					for (int k = 0; k < hull.size().height; k++) {
						int index = (int) hull.get(k, 0)[0];
						double[] point = new double[] { contour.get(index, 0)[0], contour.get(index, 0)[1] };

						left = (float) Math.min(left, point[0]);
						right = (float) Math.max(right, point[0]);
						top = (float) Math.max(top, point[1]);
						botton = (float) Math.min(botton, point[1]);
					}

					Mat p1 = bw2(image.submat(new Rect((int) cx - 25,(int) cy - 25, 50, 50)), 200);
					Mat p2 = mask.submat(new Rect((int) cx - 25, (int) cy - 25, 50, 50));
					Mat p3 = new Mat();
					Core.multiply(p1, p2, p3);
					//Mat p4 = p3;//bw(p3, 200);
					Mat p5 = new Mat();
					Core.multiply(p2, new Scalar(255), p5);

					// TODO: Use later for detect piece color
					
					int countBlack = 0;
					int countWhite = 0;
					for (int row = 0; row < p3.size().height; row++) {
						for (int col = 0; col < p3.size().width; col++) {
							if (p2.get(row, col)[0] == 1) {
								if (p3.get(row, col)[0] == 255) {
									countWhite++;
								} else {
									countBlack++;
								}
							}
						}
					}

					String pos = String.format("%d%d", i, j);
					out_color.put(key, (1.0f * countWhite / countBlack < 1.2f ? "B" : "W"));
					out_piece.put(key, p5);

					// System.out.println(String.format("%dx%d -> %f", i, j,
					// 1.0f*countWhite/countBlacl));

					// p4 = bw(p3, 200);
					// Core.add(p4, new Scalar(255), p4);

					// Mat p5 = new Mat(p4.size(), p4.type());
					// Core.multiply(p4, p2, p5);
					// System.out.println(p3.dump());

					// System.out.println(rect);

					// float side = (float)
					// Math.sqrt(Imgproc.contourArea(contour));
					Highgui.imwrite(String.format("%s/cell_%d%d_%s.png", "/root/debug", i, j, (1.0f*countWhite/countBlack < 1.2f ? "black" : "white")), p5);
					// System.out.println(String.format("%s/%d.png", dir,
					// name_index++));

					//Highgui.imwrite(String.format("%s/%d.png", dir,
					// name_index++), p5);
				}
			}
		}
	}

	private static  List<Integer> hist(Mat image) {

		List<Mat> images = new ArrayList<Mat>();
		images.add(image);

		Mat histogram = new Mat();
		MatOfFloat ranges = new MatOfFloat(0, 256);
		MatOfInt histSize = new MatOfInt(255);

		Imgproc.calcHist(images, new MatOfInt(0), new Mat(), histogram,
				histSize, ranges);

		System.out.println(histogram.size());
		// System.out.println(histogram.dump());

		// Create space for histogram image
		Mat histImage = Mat.zeros(100, (int) histSize.get(0, 0)[0],
				CvType.CV_8UC1);
		// Normalize histogram
		Core.normalize(histogram, histogram, 0, 100, Core.NORM_MINMAX, -1,
				new Mat());
		// System.out.println(histogram.dump());
		// Draw lines for histogram points
		for (int i = 0; i < (int) histSize.get(0, 0)[0]; i++) {
			Core.line(
					histImage,
					new org.opencv.core.Point(i, histImage.rows()),
					new org.opencv.core.Point(i, histImage.rows()
							- Math.round(histogram.get(i, 0)[0])), new Scalar(
							255, 255, 255), 1, 8, 0);
		}
		//imshow(histImage, "hist");

		List<Integer> pos_x = new ArrayList<Integer>();

		int mean = 50;
		int peaks = 0;
		boolean peak = false;

		for (int i = 0; i < histImage.size().width; i++) {
			double color = histImage.get(mean, i)[0];
			double colorAbove = histImage.get(mean + 5, i)[0];
			double colorBelow = histImage.get(mean - 5, i)[0];

			if (!peak && color > 0
					&& (color == colorAbove && color == colorBelow)) {
				peak = true;
				peaks++;
				pos_x.add(i);
			} else if (peak && color == 0
					&& (color == colorAbove && color == colorBelow)) {
				peak = false;
			}
		}

		return pos_x;
	}

	private static  Mat submat(Mat image, MatOfPoint contour) {
		double lt[] = contour.get(0, 0);
		double rt[] = contour.get(1, 0);
		double rb[] = contour.get(2, 0);
		double lb[] = contour.get(3, 0);

		double width = rt[0] - lt[0];
		double height = lt[1] - lb[1];

		Mat s = image.submat((int) lb[1], (int) (lb[1] + height), (int) lb[0],
				(int) (lb[0] + width));
		return s;
	}

	private static  List<Mat> recognizeCells(Mat image) {		
		Mat open = morph(image, Imgproc.MORPH_OPEN, Imgproc.MORPH_RECT, 5);
		open = morph(open, Imgproc.MORPH_OPEN, Imgproc.MORPH_ELLIPSE, 5);

		float margin = -1;
		for (int i = 100; i < 200; i = i + 20) {
			Mat binary = bw2(open, i);
			Mat temp = new Mat(image.size(), image.type());
			margin = margin(binary, temp);
			System.out.println("margin: " + margin);

			if (margin > -1) {
				break;
			}
		}

		Mat grid = Mat.ones(image.size(), CvType.CV_8U);
		List<MatOfPoint> contours = cells(image, grid, margin);
		Mat pieces = recognizeChessPieces(image, grid);

		List<Mat> split_pieces = new ArrayList<Mat>();
		for (int i = 0; i < contours.size(); i++) {
			Mat mask = Mat.zeros(pieces.size(), CvType.CV_8U);

			Scalar white = new Scalar(1);
			Imgproc.drawContours(mask, contours, i, white, Core.FILLED);

			Mat cell = new Mat();
			Core.multiply(pieces, mask, cell);

			split_pieces.add(cell);
		}
		
		return split_pieces;
	}

	private static  List<Mat> recognizeCells2(Mat image) {		
		Mat open = morph(image, Imgproc.MORPH_OPEN, Imgproc.MORPH_RECT, 5);
		open = morph(open, Imgproc.MORPH_OPEN, Imgproc.MORPH_ELLIPSE, 5);

		float margin = -1;
		for (int i = 100; i < 200; i = i + 20) {
			Mat binary = bw2(open, i);
			Mat temp = new Mat(image.size(), image.type());
			margin = margin(binary, temp);
			System.out.println("margin: " + margin);

			if (margin > -1) {
				break;
			}
		}

		Mat temp = new Mat(image.size(), image.type());
		List<MatOfPoint> contours = cells(image, temp, margin);
		
		if (contours == null){
			return null;
		}

		Mat pieces = recognizeChessPieces2(image, true);
		imshow(pieces, "pieces");

		List<Mat> split_pieces = new ArrayList<Mat>();
		Mat drawing = image.clone();
		
		for (int i = 0; i < contours.size(); i++) {
			Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
			Imgproc.drawContours(drawing, contours, i, color, Core.LINE_4);

			Mat mask = Mat.zeros(image.size(), CvType.CV_8UC3);
			
			Scalar white = new Scalar(1, 1, 1);
			Imgproc.drawContours(mask, contours, i, white, Core.FILLED);

			Mat cell = new Mat();
			Core.multiply(pieces, mask, cell);

			split_pieces.add(cell);
		}
		
		return split_pieces;

	}
	
	private static Mat pieces2(Mat image){
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

		Mat drawing = Mat.zeros(image.size(), CvType.CV_8UC3);
		for (int i = 0; i < contours.size(); i++) {
			MatOfPoint contour = contours.get(i);
			float area = (float) Imgproc.contourArea(contour);

			MatOfInt hull = new MatOfInt();
			Imgproc.convexHull(contour, hull);

			MatOfPoint hull_curve = new MatOfPoint();
			hull_curve.create((int) hull.size().height, 1, CvType.CV_32SC2);

			for (int j = 0; j < hull.size().height; j++) {
				int index = (int) hull.get(j, 0)[0];
				double[] point = new double[] { contour.get(index, 0)[0], contour.get(index, 0)[1] };
				hull_curve.put(j, 0, point);
			}

			float hull_area = (float) Imgproc.contourArea(hull_curve);
			MatOfPoint2f contour2f = new MatOfPoint2f();
			contour2f.fromArray(contour.toArray());
			float length = (float) Imgproc.arcLength(contour2f, true);

			Rect rect = Imgproc.boundingRect(contour);
			float diff = 1.0f * Math.max(rect.width, rect.height) / Math.min(rect.width, rect.height);

			//if (area > 400 && hull_area < 2000 && length < 400 && diff < 2)
			if (area > 300 && hull_area < 2000 && length < 400 && diff < 2)
			{
				Scalar color = new Scalar(255, 255, 255);
				//Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
				Imgproc.drawContours(drawing, contours, i, color, Core.FILLED, 8, hierarchy, 0, new Point());
			}
		}
		
		return drawing;
	}
	
	private static Mat recognizeChessPieces2(Mat image, boolean filled) {
		Mat binary1 = bw2(image, 90);
		Mat binary2 = bw2(image, 130);
		
		Mat drawing1 = pieces2(binary1);
		Mat drawing2 = pieces2(binary2);
		
		Mat drawing = new Mat();
		Core.add(drawing1, drawing2, drawing);
		return drawing;
	}

	
	private  void normalize(MatOfPoint external_contour) {
		double lt[] = external_contour.get(0, 0);
		double rt[] = external_contour.get(1, 0);
		double rb[] = external_contour.get(2, 0);
		double lb[] = external_contour.get(3, 0);

		double width = rt[0] - lt[0];
		double height = lt[1] - lb[1];

		external_contour.put(0, 0, new double[] { 0, 0 + height });
		external_contour.put(1, 0, new double[] { width, 0 + height });
		external_contour.put(2, 0, new double[] { width, 0 });
		external_contour.put(3, 0, new double[] { 0, 0 });		
	}

	private static  MatOfPoint warpPerspective(Mat image, Mat dest,
			MatOfPoint contour) {
		double lt[] = contour.get(0, 0);
		double rt[] = contour.get(1, 0);
		double rb[] = contour.get(2, 0);
		double lb[] = contour.get(3, 0);

		Mat src = new Mat(4, 1, CvType.CV_32FC2);
		src.put(0, 0, lt);
		src.put(1, 0, rt);
		src.put(2, 0, rb);
		src.put(3, 0, lb);

		double width = rt[0] - lt[0];
		double height = lt[1] - lb[1];

		Mat dst = new Mat(4, 1, CvType.CV_32FC2);
		dst.put(0, 0, new double[] { lb[0], lb[1] + height });
		dst.put(1, 0, new double[] { lb[0] + width, lb[1] + height });
		dst.put(2, 0, new double[] { lb[0] + width, lb[1] });
		dst.put(3, 0, new double[] { lb[0], lb[1] });

		// Get transformation matrix
		Mat transformation = Imgproc.getPerspectiveTransform(src, dst);

		// Apply perspective transformation
		Imgproc.warpPerspective(image, dest, transformation, dest.size());

		Mat m = dest.submat((int) lb[1], (int) (lb[1] + height), (int) lb[0],
				(int) (lb[0] + width));
		// Highgui.imwrite("/root/86.png", m);

		MatOfPoint rect = new MatOfPoint();
		rect.create(4, 1, CvType.CV_32SC2);
		rect.put(0, 0, dst.get(0, 0));
		rect.put(1, 0, dst.get(1, 0));
		rect.put(2, 0, dst.get(2, 0));
		rect.put(3, 0, dst.get(3, 0));

		return rect;
	}

	private static  float calculateScale(Mat image) {
		Mat morphology = morph(image, Imgproc.MORPH_OPEN, Imgproc.MORPH_RECT, 5);
		Mat edges = edges(morphology);

		// imshow(edges, "imshow(edges);");

		// Mat board = Mat.zeros(edges.size(), CvType.CV_8UC3);
		// board(edges, true, board);

		// Mat image8u = new Mat();
		// Imgproc.cvtColor(image, image8u, Imgproc.COLOR_BGR2GRAY);

		Mat im_bw = edges.clone();
		// Imgproc.threshold(image8u, im_bw, 128, 255, Imgproc.THRESH_BINARY |
		// Imgproc.THRESH_OTSU);
		// Imgproc.blur(image8u, image8u, new Size(1, 1));

		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(im_bw, contours, hierarchy, Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

		List<MatOfPoint> rect_contours = new ArrayList<MatOfPoint>();

		for (int i = 0; i < contours.size(); i++) {
			MatOfPoint contour = contours.get(i);

			if (Imgproc.contourArea(contour) > 0.1f) {

				float xl = Float.MAX_VALUE;
				float xr = Float.MIN_VALUE;
				float yb = Float.MAX_VALUE;
				float yt = Float.MIN_VALUE;

				for (int j = 0; j < contour.size().height; j++) {
					double xy[] = contour.get(j, 0);

					if (xy[0] < xl) {
						xl = (float) xy[0];
					} else if (xy[0] > xr) {
						xr = (float) xy[0];
					}

					if (xy[1] < yb) {
						yb = (float) xy[1];
					} else if (xy[1] > yt) {
						yt = (float) xy[1];
					}
				}

				MatOfPoint rect = new MatOfPoint();
				rect.create(4, 1, CvType.CV_32SC2);

				double lt[] = { xl, yt };
				rect.put(0, 0, lt);

				double rt[] = { xr, yt };
				rect.put(1, 0, rt);

				double rb[] = { xr, yb };
				rect.put(2, 0, rb);

				double lb[] = { xl, yb };
				rect.put(3, 0, lb);

				rect_contours.add(rect);
			}
		}

		List<Float> areas = new ArrayList<Float>();
		for (int i = 0; i < rect_contours.size(); i++) {
			MatOfPoint contour = rect_contours.get(i);
			float area = (float) Imgproc.contourArea(contour);
			areas.add(area);
		}

		float max = Collections.max(areas);

		float scale = (float) Math.sqrt(141376 / max);
		System.out.println("SCALE: " + scale);

		return scale;
	}

	private static MatOfPoint board(Mat image, Mat mask) {
		Mat im_bw = image.clone();
		Mat temp = Mat.zeros(image.size(), image.type());
		// mask = Mat.zeros(image.size(), CvType.CV_8U);
		Imgproc.blur(im_bw, im_bw, new Size(3, 3));
		
		// find external counter
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(im_bw, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

		float max_area = Float.MIN_VALUE;

		for (int i = 0; i < contours.size(); i++) {
			MatOfPoint contour = contours.get(i);
			float area = (float) Imgproc.contourArea(contour);
			if (area > max_area) {
				max_area = area;
				// max_contour = contour;
			}
		}

		// draw external contour
		for (int i = 0; i < contours.size(); i++) {
			MatOfPoint contour = contours.get(i);
			float area = (float) Imgproc.contourArea(contour);

			if (area == max_area) {
				Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
				Imgproc.drawContours(temp, contours, i, color, Core.LINE_4, 8, hierarchy, 0, new Point());
				break;
			}
		}

		// find corners
		Mat corners = blackhat(temp, 3);
		//imshow(bw2(corners, 50), "corners");

		contours = new ArrayList<MatOfPoint>();
		hierarchy = new Mat();
		Imgproc.findContours(corners, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE, new Point(0, 0));
		
		if (contours.size() == 0){
			return null;
		}

		// make approximation of external contour
		List<MatOfPoint> rect_contours = new ArrayList<MatOfPoint>();
		List<Point> vertices = new ArrayList<Point>();
		float cx = 0;
		float cy = 0;
		
		for (int i = 0; i < contours.size(); i++) {
			MatOfPoint contour = contours.get(i);
			Moments moments = Imgproc.moments(contour);
			float x = (float) (moments.get_m10() / moments.get_m00());
			float y = (float) (moments.get_m01() / moments.get_m00());
			vertices.add(new Point(x, y));

			cx += x;
			cy += y;
		}

		List<Point> left = new ArrayList<Point>();
		List<Point> right = new ArrayList<Point>();

		cx = cx / contours.size();
		cy = cy / contours.size();

		for (Point p : vertices) {
			if (p.x < cx) {
				left.add(p);
			} else {
				right.add(p);
			}
		}

		if (left.size() < 2 || right.size() < 2){
			return null;
		}
		
		Point lt = left.get(0);
		Point lb = left.get(0);
		Point rt = right.get(0);
		Point rb = right.get(0);

		for (Point p : left) {
			if (p.y < lb.y) {
				lb = p;
			} else if (p.y > lt.y) {
				lt = p;
			}
		}

		for (Point p : right) {
			if (p.y < rb.y) {
				rb = p;
			} else if (p.y > rt.y) {
				rt = p;
			}
		}

		MatOfPoint rect = new MatOfPoint();
		rect.create(4, 1, CvType.CV_32SC2);
		rect.put(0, 0, new double[] { lt.x, lt.y });
		rect.put(1, 0, new double[] { rt.x, rt.y });
		rect.put(2, 0, new double[] { rb.x, rb.y });
		rect.put(3, 0, new double[] { lb.x, lb.y });
		rect_contours.add(rect);

		for (int i = 0; i < rect_contours.size(); i++) {
			Scalar color = new Scalar(1, 1, 1);
			Imgproc.drawContours(mask, rect_contours, i, color, Core.FILLED, 8,
					hierarchy, 0, new Point());
		}

		return rect;
	}

	private static  Mat morph(Mat image, int operation, int element,
			int kernel_size) {
		// Mat image8u = new Mat();
		// Imgproc.cvtColor(image, image8u, Imgproc.COLOR_BGR2GRAY);

		Mat im_bw = image.clone();
		// Imgproc.threshold(image8u, im_bw, 128, 255, Imgproc.THRESH_BINARY |
		// Imgproc.THRESH_OTSU);

		// void Morphology_Operations( int, void* )
		{
			// Since MORPH_X : 2,3,4,5 and 6
			// int operation = morph_operator + 2;

			Mat el = Imgproc.getStructuringElement(element, new Size(
					2 * kernel_size + 1, 2 * kernel_size + 1), new Point(
					kernel_size, kernel_size));

			// / Apply the specified morphology operation
			Mat morph = new Mat();
			Imgproc.morphologyEx(im_bw, morph, operation, el);
			return morph;
		}
	}

	private static  Mat blackhat(Mat image, int kernel_size) {
		// Mat image8u = new Mat();
		// Imgproc.cvtColor(image, image8u, Imgproc.COLOR_BGR2GRAY);

		Mat im_bw = image.clone();
		// Imgproc.threshold(image8u, im_bw, 128, 255, Imgproc.THRESH_BINARY |
		// Imgproc.THRESH_OTSU);

		// void Morphology_Operations( int, void* )
		{
			// Since MORPH_X : 2,3,4,5 and 6
			// int operation = morph_operator + 2;

			Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS,
					new Size(2 * kernel_size + 1, 2 * kernel_size + 1),
					new Point(kernel_size, kernel_size));

			// / Apply the specified morphology operation
			Mat morph = new Mat();
			Imgproc.morphologyEx(im_bw, morph, Imgproc.MORPH_BLACKHAT, element);
			return morph;
		}
	}

	private static  Mat edges(Mat image) {
		// Mat image8u = new Mat(image.size(), image.type());
		// Imgproc.cvtColor(image, image8u, Imgproc.COLOR_BGR2GRAY);

		Mat im_bw = image.channels() > 1 ? bw(image, 200) : image;
		// Imgproc.threshold(image8u, im_bw, 128, 255, Imgproc.THRESH_BINARY |
		// Imgproc.THRESH_OTSU);
		// Imgproc.blur(image8u, image8u, new Size(1, 1));

		Mat edges = new Mat(image.size(), image.type());

		float thresh = 100;
		Imgproc.Canny(im_bw, edges, thresh, thresh * 2);

		return edges;
	}

	private static  Mat bw(Mat image, double thresh) {
		Mat image8u = new Mat();
		if (image.channels() > 1) {
			Imgproc.cvtColor(image, image8u, Imgproc.COLOR_BGR2GRAY);
		}

		// imshow(image8u, "gray");

		Mat im_bw = new Mat();
		Imgproc.threshold(image8u, im_bw, thresh, 255, Imgproc.THRESH_BINARY
				| Imgproc.THRESH_OTSU);
		// Imgproc.blur(image8u, image8u, new Size(1, 1));

		return im_bw;
	}

	private static  Mat gray(Mat image) {
		Mat image8u = new Mat(image.size(), CvType.CV_8U);
		Imgproc.cvtColor(image, image8u, Imgproc.COLOR_BGR2GRAY);
		return image8u;
	}

	private static  Mat bw2(Mat image, double thresh) {
		Mat gray = new Mat();

		if (image.channels() > 1) {
			Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
		} else {
			gray = image.clone();
		}

		Mat binary = new Mat(gray.size(), CvType.CV_8U);

		for (int i = 0; i < gray.size().height; i++) {
			for (int j = 0; j < gray.size().width; j++) {
				binary.put(i, j, gray.get(i, j)[0] < thresh ? 0 : 255);
			}
		}

		// Imgproc.threshold(image8u, im_bw, thresh, 255,
		// Imgproc.THRESH_BINARY);
		// Imgproc.blur(image8u, image8u, new Size(1, 1));

		return binary;
	}

	private  Mat filter(Mat image, double thresh) {
		Mat image_f = new Mat(image.size(), CvType.CV_8U);

		for (int i = 0; i < image.size().height; i++) {
			for (int j = 0; j < image.size().width; j++) {
				double intens = image.get(i, j)[0];
				image_f.put(i, j, intens < thresh ? image.get(i, j)[0] : 255);
			}
		}

		return image_f;
	}
	
	private static  float margin(Mat image, Mat dest) {
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();

		Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

		List<MatOfPoint> rect_contours = new ArrayList<MatOfPoint>();
		List<Float> areas = new ArrayList<Float>();

		for (int i = 0; i < contours.size(); i++) {
			MatOfPoint contour = contours.get(i);

			float xl = Float.MAX_VALUE;
			float xr = Float.MIN_VALUE;
			float yb = Float.MAX_VALUE;
			float yt = Float.MIN_VALUE;

			for (int j = 0; j < contour.size().height; j++) {
				double xy[] = contour.get(j, 0);

				if (xy[0] < xl) {
					xl = (float) xy[0];
				} else if (xy[0] > xr) {
					xr = (float) xy[0];
				}

				if (xy[1] < yb) {
					yb = (float) xy[1];
				} else if (xy[1] > yt) {
					yt = (float) xy[1];
				}
			}

			float area = (xr - xl) * (yt - yb);
			float ratio = (xr - xl) / (yt - yb);

			if (area > 1000 && area < 3000 && ratio > 0.9 && ratio < 1.1) {
				areas.add(area);

				MatOfPoint rect = new MatOfPoint();
				rect.create(4, 1, CvType.CV_32SC2);

				double lt[] = { xl, yt };
				rect.put(0, 0, lt);

				double rt[] = { xr, yt };
				rect.put(1, 0, rt);

				double rb[] = { xr, yb };
				rect.put(2, 0, rb);

				double lb[] = { xl, yb };
				rect.put(3, 0, lb);

				rect_contours.add(rect);
			}
		}

		if (areas.size() == 0) {
			return -1;
		}

		Collections.sort(areas);
		int index = areas.size() / 2;
		float medium = areas.get(index);

		for (int i = 0; i < rect_contours.size(); i++) {
			MatOfPoint contour = rect_contours.get(i);
			float area = (float) Imgproc.contourArea(contour);

			if ((area < -medium * 0.1f + medium || area > medium * 0.1f
					+ medium)) {
				rect_contours.remove(contour);
			}
		}

		for (int i = 0; i < rect_contours.size(); i++) {
			MatOfPoint contour = rect_contours.get(i);
			Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
			Imgproc.drawContours(dest, rect_contours, i, color, Core.LINE_4, 8, hierarchy, 0, new Point());
		}

		// imshow(dest, "immmmm2");

		float left = 0;
		float right = image.width();
		float top = image.height();
		float botton = 0;

		float margin = Float.MAX_VALUE;

		for (int i = 0; i < rect_contours.size(); i++) {
			MatOfPoint contour = rect_contours.get(i);

			double lt[] = contour.get(0, 0);
			double rt[] = contour.get(1, 0);
			double rb[] = contour.get(2, 0);
			double lb[] = contour.get(3, 0);

			margin = (float) Math.min(margin, lt[0] - left);
			margin = (float) Math.min(margin, right - rt[0]);
			margin = (float) Math.min(margin, top - lt[1]);
			margin = (float) Math.min(margin, lb[1] - botton);
		}

		return margin < Math.max(right - left, top - botton) / 16 ? margin : -1;
	}

	private static  List<MatOfPoint> cells(Mat image, Mat dest, float margin) {
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

		double lt[] = new double[] {0, image.height()};
		double rt[] = new double[] {image.width(), image.height()};
		double rb[] = new double[] {image.width(), 0};
		double lb[] = new double[] {0, 0};

		double dx = (rb[0] - lb[0] - 2 * margin) / 8;
		double dy = (lb[1] - lt[1] + 2 * margin) / 8;

		double m = 0.0;
		
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				double p00[] = new double[] {
						lt[0] + j * dx + margin - dx * m,
						lt[1] + i * dy - margin - dy * m };
				double p10[] = new double[] {
						lt[0] + (j + 1) * dx + margin + dx * m,
						lt[1] + i * dy - margin - dy * m };
				double p11[] = new double[] {
						lt[0] + (j + 1) * dx + margin + dx * m,
						lt[1] + (i + 1) * dy - margin + dy * m };
				double p01[] = new double[] {
						lt[0] + j * dx + margin - dx * m,
						lt[1] + (i + 1) * dy - margin + dy * m };

				MatOfPoint cell = new MatOfPoint();
				cell.create(4, 1, CvType.CV_32SC2);
				cell.put(0, 0, p00);
				cell.put(1, 0, p10);
				cell.put(2, 0, p11);
				cell.put(3, 0, p01);

				contours.add(cell);
			}
		}

		Mat hierarchy = new Mat();
		for (int i = 0; i < contours.size(); i++) {
			//Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
			//Scalar color = new Scalar(0, 0, 0);
			Scalar color = new Scalar(0);
			Imgproc.drawContours(dest, contours, i, color, Core.LINE_4, 8, hierarchy, 0, new Point());
		}

		//imshow(dest, "cells");

		return contours;
	}
	
	private static void recognizeChessPieces2(Mat image, Map<String, Mat> out_color, Map<String, Mat> out_piece) {
		List<Mat> split_pieces = recognizeCells2(image);
		
		Map<String, Mat> cleaned_pieces = new HashMap<String, Mat>();
		Map<String, MatOfPoint> pieces_contour = new HashMap<String, MatOfPoint>();
		
		for (int i = 0; i < split_pieces.size(); i++) {
			Mat piece = split_pieces.get(i);
		
			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			Mat hierarchy = new Mat();
			
			Imgproc.findContours(bw(piece, 200), contours, hierarchy,Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));
			Mat drawing = Mat.zeros(piece.size(), CvType.CV_8U);

			List<MatOfPoint> filtred_contours = new ArrayList<MatOfPoint>();
			for (int j = 0; j < contours.size(); j++) {
				MatOfPoint contour = contours.get(j);
				float area = (float) Imgproc.contourArea(contour);

				if (area > 400) {
					filtred_contours.add(contour);
				}
			}

			for (int j = 0; j < filtred_contours.size(); j++) {
				Scalar color = new Scalar(1);
				Imgproc.drawContours(drawing, filtred_contours, j, color, Core.FILLED);
			}

			if (filtred_contours.size() > 0) {
				cleaned_pieces.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1), drawing);
				pieces_contour.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1), filtred_contours.get(0));
			}
		}

		image = bw(image, 200);
		//imshow(image, "binary");
		
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", i, j);
				if (cleaned_pieces.containsKey(key)) {
					Mat piece = new Mat(image.size(), image.type());
					Mat mask = cleaned_pieces.get(key);
					Core.multiply(image, mask, piece);

					MatOfPoint contour = pieces_contour.get(key);
					Moments moments = Imgproc.moments(contour);
					int cx = (int) (moments.get_m10() / moments.get_m00());
					int cy = (int) (moments.get_m01() / moments.get_m00());

					MatOfInt hull = new MatOfInt();
					Imgproc.convexHull(contour, hull);

					// MatOfPoint hull_curve = new MatOfPoint();
					// hull_curve.create((int) hull.size().height, 1,
					// CvType.CV_32SC2);
					float left = Float.MAX_VALUE;
					float right = Float.MIN_VALUE;
					float top = Float.MIN_VALUE;
					float botton = Float.MAX_VALUE;

					for (int k = 0; k < hull.size().height; k++) {
						int index = (int) hull.get(k, 0)[0];
						double[] point = new double[] { contour.get(index, 0)[0], contour.get(index, 0)[1] };

						left = (float) Math.min(left, point[0]);
						right = (float) Math.max(right, point[0]);
						top = (float) Math.max(top, point[1]);
						botton = (float) Math.min(botton, point[1]);
					}
					
					//System.out.println("image size: " + image.size());
					//System.out.println(String.format("cx, cy = <%d,%d>", cx, cy));
					//System.out.println(Math.min(cy + 25, image.height()-1));
					//System.out.println(Math.max(0, cx - 25));
					
					int rowStart = Math.max(0, cy - 25);
					int rowEnd = Math.min(cy + 25, image.height()-1);
					int columnStart = Math.max(0, cx - 25);
					int columnEnd = Math.min(cx + 25, image.width()-1);
					
					Mat p1 = bw2(image.submat(rowStart, rowEnd, columnStart, columnEnd), 200);
					Mat p2 = mask.submat(rowStart, rowEnd, columnStart, columnEnd);
					Mat p3 = new Mat();
					Core.multiply(p1, p2, p3);
					//Mat p4 = p3;//bw(p3, 200);
					Mat p5 = new Mat();
					Core.multiply(p2, new Scalar(255), p5);

					// TODO: Use later for detect piece color
					
					float countBlack = 0;
					float countWhite = 0;
					for (int row = 0; row < p3.size().height; row++) {
						for (int col = 0; col < p3.size().width; col++) {
							//if (p2.get(row, col)[0] == 1) {
								if (p3.get(row, col)[0] == 255) {
									countWhite++;
								} else {
									countBlack++;
								}
							//}
						}
					}

					//imshow(p3, String.format("p3_%f", countWhite / countBlack));
					String pos = String.format("%d%d", i, j);
					//System.err.println(countWhite / countBlack);
					out_color.put(key, p3);
					out_piece.put(key, p5);

					// System.out.println(String.format("%dx%d -> %f", i, j,
					// 1.0f*countWhite/countBlacl));

					// p4 = bw(p3, 200);
					// Core.add(p4, new Scalar(255), p4);

					// Mat p5 = new Mat(p4.size(), p4.type());
					// Core.multiply(p4, p2, p5);
					// System.out.println(p3.dump());

					// System.out.println(rect);

					// float side = (float)
					// Math.sqrt(Imgproc.contourArea(contour));
					//Highgui.imwrite(String.format("%s/cell_%d%d_%s.png", "/root/debug", i, j, (1.0f*countWhite/countBlack < 1.2f ? "black" : "white")), p5);
					// System.out.println(String.format("%s/%d.png", dir,
					// name_index++));

					//Highgui.imwrite(String.format("%s/%d.png", dir,
					// name_index++), p5);
				}
			}
		}

	}
	
	private Integer recognizeChessPieces2(Mat image, String dir, Integer name_index) {
		List<Mat> split_pieces = recognizeCells2(image);
		
		Map<String, Mat> cleaned_pieces = new HashMap<String, Mat>();
		Map<String, MatOfPoint> pieces_contour = new HashMap<String, MatOfPoint>();
		
		for (int i = 0; i < split_pieces.size(); i++) {
			Mat piece = split_pieces.get(i);
		
			List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
			Mat hierarchy = new Mat();
			
			Imgproc.findContours(bw(piece, 200), contours, hierarchy,Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));
			Mat drawing = Mat.zeros(piece.size(), CvType.CV_8U);

			List<MatOfPoint> filtred_contours = new ArrayList<MatOfPoint>();
			for (int j = 0; j < contours.size(); j++) {
				MatOfPoint contour = contours.get(j);
				float area = (float) Imgproc.contourArea(contour);

				if (area > 400) {
					filtred_contours.add(contour);
				}
			}

			for (int j = 0; j < filtred_contours.size(); j++) {
				Scalar color = new Scalar(1);
				Imgproc.drawContours(drawing, filtred_contours, j, color, Core.FILLED);
			}

			if (filtred_contours.size() > 0) {
				cleaned_pieces.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1), drawing);
				pieces_contour.put(String.format("%dx%d", i / 8 + 1, i % 8 + 1), filtred_contours.get(0));
			}
		}

		image = bw(image, 200);
		//imshow(image, "binary");
		
		for (int i = 1; i < 9; i++) {
			for (int j = 1; j < 9; j++) {
				String key = String.format("%dx%d", i, j);
				if (cleaned_pieces.containsKey(key)) {
					Mat piece = new Mat(image.size(), image.type());
					Mat mask = cleaned_pieces.get(key);
					Core.multiply(image, mask, piece);

					MatOfPoint contour = pieces_contour.get(key);
					Moments moments = Imgproc.moments(contour);
					int cx = (int) (moments.get_m10() / moments.get_m00());
					int cy = (int) (moments.get_m01() / moments.get_m00());

					MatOfInt hull = new MatOfInt();
					Imgproc.convexHull(contour, hull);

					// MatOfPoint hull_curve = new MatOfPoint();
					// hull_curve.create((int) hull.size().height, 1,
					// CvType.CV_32SC2);
					float left = Float.MAX_VALUE;
					float right = Float.MIN_VALUE;
					float top = Float.MIN_VALUE;
					float botton = Float.MAX_VALUE;

					for (int k = 0; k < hull.size().height; k++) {
						int index = (int) hull.get(k, 0)[0];
						double[] point = new double[] { contour.get(index, 0)[0], contour.get(index, 0)[1] };

						left = (float) Math.min(left, point[0]);
						right = (float) Math.max(right, point[0]);
						top = (float) Math.max(top, point[1]);
						botton = (float) Math.min(botton, point[1]);
					}
					
					//System.out.println("image size: " + image.size());
					//System.out.println(String.format("cx, cy = <%d,%d>", cx, cy));
					//System.out.println(Math.min(cy + 25, image.height()-1));
					//System.out.println(Math.max(0, cx - 25));
					
					int rowStart = Math.max(0, cy - 25);
					int rowEnd = Math.min(cy + 25, image.height()-1);
					int columnStart = Math.max(0, cx - 25);
					int columnEnd = Math.min(cx + 25, image.width()-1);
					
					Mat p1 = bw2(image.submat(rowStart, rowEnd, columnStart, columnEnd), 200);
					Mat p2 = mask.submat(rowStart, rowEnd, columnStart, columnEnd);
					Mat p3 = new Mat();
					Core.multiply(p1, p2, p3);
					//Mat p4 = p3;//bw(p3, 200);
					Mat p5 = new Mat();
					Core.multiply(p2, new Scalar(255), p5);

					// TODO: Use later for detect piece color
					
					int countBlack = 0;
					int countWhite = 0;
					for (int row = 0; row < p3.size().height; row++) {
						for (int col = 0; col < p3.size().width; col++) {
							if (p2.get(row, col)[0] == 1) {
								if (p3.get(row, col)[0] == 255) {
									countWhite++;
								} else {
									countBlack++;
								}
							}
						}
					}

					String pos = String.format("%d%d", i, j);
					//out_color.put(key, (1.0f * countWhite / countBlack < 1.2f ? "B" : "W"));
					//out_piece.put(key, p5);

					// System.out.println(String.format("%dx%d -> %f", i, j,
					// 1.0f*countWhite/countBlacl));

					// p4 = bw(p3, 200);
					// Core.add(p4, new Scalar(255), p4);

					// Mat p5 = new Mat(p4.size(), p4.type());
					// Core.multiply(p4, p2, p5);
					// System.out.println(p3.dump());

					// System.out.println(rect);

					// float side = (float)
					// Math.sqrt(Imgproc.contourArea(contour));
					//Highgui.imwrite(String.format("%s/cell_%d%d_%s.png", "/root/debug", i, j, (1.0f*countWhite/countBlack < 1.2f ? "black" : "white")), p5);
					// System.out.println(String.format("%s/%d.png", dir,
					// name_index++));

					Highgui.imwrite(String.format("%s/%d.png", dir, name_index++), p3);
					System.err.println(Integer.toString(name_index));
				}
			}
		}

		return name_index;
	}


	private static  Mat recognizeChessPieces(Mat image, Mat grid) {
		List<Integer> peaks = hist(image);
		Mat bw = null;

		imshow(image, "image");
		
		//if (peaks.size() == 2) {
			Mat destination = new Mat(image.size(), image.type());
			Imgproc.GaussianBlur(image, destination, new Size(0, 0), 10);
			Core.addWeighted(image, 1.5, destination, -0.5, 0, destination);

			imshow(destination, "sharpness");

			Mat edges = new Mat(image.size(), CvType.CV_8U);
			edges = edges(gray(image));
			
			bw = bw2(edges, 200).mul(grid);
			bw = morph(bw, Imgproc.MORPH_CLOSE, Imgproc.MORPH_RECT, 1);

			imshow(bw, "bw_morph");
		//}

		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(bw, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

		Mat drawing = Mat.zeros(image.size(), CvType.CV_8U);

		for (int j = 0; j < contours.size(); j++) {
			MatOfPoint contour = contours.get(j);
			double area = Imgproc.contourArea(contour);

			MatOfInt hull = new MatOfInt();
			Imgproc.convexHull(contour, hull);

			MatOfPoint hull_curve = new MatOfPoint();
			hull_curve.create((int) hull.size().height, 1, CvType.CV_32SC2);

			for (int k = 0; k < hull.size().height; k++) {
				int index = (int) hull.get(k, 0)[0];
				double[] point = new double[] { contour.get(index, 0)[0], contour.get(index, 0)[1] };
				hull_curve.put(k, 0, point);
			}

			MatOfPoint2f contour2f = new MatOfPoint2f();
			contour2f.fromArray(contour.toArray());
			
			if (area > 50)
			{
				//Scalar color = new Scalar(Math.random() * 255, Math.random() * 255, Math.random() * 255);
				// Scalar color = new Scalar(1, 1, 1);
				Scalar color = new Scalar(255);
				Imgproc.drawContours(drawing, contours, j, color, Core.FILLED);
			}
		}

		imshow(drawing, "contours");
		return drawing;
	}

	private  Map<String, Integer> loadIndexes(String path) {
		String index_file = String.format("%s/indexes", path);
		Map<String, Integer> indexes = new HashMap<String, Integer>();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(index_file));
			String line = null;

			while ((line = reader.readLine()) != null) {
				int splitter = line.indexOf(":");
				String name = line.substring(0, splitter);
				int index = Integer.parseInt(line.substring(splitter + 1));

				indexes.put(name, index);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return indexes;
	}
	
	private  List<Integer> loadValidationIndexes(String path) {
		String index_file = String.format("%s/traning_indexes", path);
		List<Integer> indexes = new ArrayList<Integer>();

		try {
			BufferedReader reader = new BufferedReader(new FileReader(index_file));
			String line = null;

			while ((line = reader.readLine()) != null) {
				indexes.add(Integer.parseInt(line));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return indexes;
	}


	private  Map<String, Mat> loadImages(Map<String, Integer> indexes, String dir) {
		Map<String, Mat> images = new HashMap<String, Mat>();
		//String dir = "/root/chess-samples/training-set";

		for (File file : new File(dir).listFiles()) {
			if (file.getAbsolutePath().endsWith(".png")) {
				Mat image = Highgui.imread(file.getAbsolutePath());
				String name = file.getName().replace(".png", "");

				//if (indexes.containsKey(name)) 
				{
					images.put(name, image);
				}
			}
		}

		return images;
	}

	private  void prepareFeatureVectors(Map<String, Mat> images,
			Map<String, Integer> indexes, Mat traning_set,
			Mat traning_responce, Mat data_set, Mat data_responce,
			List<String> traning_set_indexes, int end_training_set) {

		int index = 0;

		for (String key : images.keySet()) {
			Mat image = images.get(key);
			Imgproc.resize(image, image, new Size(20, 20));
			//Highgui.imwrite("/root/cells_20/" + index + ".png", image);

			Mat image_bw = bw(image, 200);
			Core.multiply(image_bw, new Scalar(1.0f / 255, 1.0f / 255, 1.0f / 255), image_bw);

			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 20; j++) {
					if (index < end_training_set) {
						traning_set.put(index, i * 20 + j, image_bw.get(i, j));
					} else {
						data_set.put(index - end_training_set, i * 20 + j, image_bw.get(i, j));
					}
				}
			}

			if (index < end_training_set) {
				traning_set_indexes.add(key);
				traning_responce.put(index, 0, (int) indexes.get(key));
			} else {
				data_responce.put(index - end_training_set, 0, (int) indexes.get(key));
			}

			index++;
			System.out.println(index + " of " + images.size());
		}
	}
	
	private void prepareFeatureVectors(Map<String, Mat> images,
			Map<String, Integer> indexes, List<Integer> validation_indexes,
			Mat data_set, Mat data_responce) {

		int index = 0;

		for (String key : images.keySet()) {
			if (!validation_indexes.contains(Integer.parseInt(key))) {

				Mat image = images.get(key);
				Imgproc.resize(image, image, new Size(20, 20));

				Mat image_bw = bw(image, 200);
				Core.multiply(image_bw, new Scalar(1.0f / 255, 1.0f / 255, 1.0f / 255), image_bw);

				for (int i = 0; i < 20; i++) {
					for (int j = 0; j < 20; j++) {
						data_set.put(index, i * 20 + j, image_bw.get(i, j));
					}
				}

				data_responce.put(index, 0, (int) indexes.get(key));

				index++;
				System.out.println(index + " of " + (indexes.size() - validation_indexes.size()));
			}
		}
	}


	private  CvNormalBayesClassifier train(String dir, Mat traning_set, Mat traning_responce) {
		String classifier_file = String.format("%s/classifier.rules", dir);
		CvNormalBayesClassifier classifier = new CvNormalBayesClassifier();
		classifier.train(traning_set, traning_responce);
		return classifier;
	}

	private  void writeClassifierToFile(
			CvNormalBayesClassifier classifier, String dir) {
		String classifier_file = String.format("%s/classifier.rules", dir);
		classifier.save(classifier_file);
	}

	private  void writeTrainSetIndexes(String dir, List<String> traning_set_indexes) {
		try {
			String traning_index_file = String.format("%s/traning_indexes", dir);
			BufferedWriter writer = new BufferedWriter(new FileWriter(traning_index_file));

			// write traning set indexes
			for (int i = 0; i < traning_set_indexes.size(); i++) {
				writer.write(traning_set_indexes.get(i));
				writer.write("\n");
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static  void imshow(Mat image, String title) {
		//TODO: Stub
	}

}
