package com.olsen.easychess.camera;

import java.io.File;
import java.util.ArrayList;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import com.olsen.easychess.Chessboard;
import com.olsen.easychess.Chesspiece;
import com.olsen.easychess.R;
import com.olsen.easychess.drawing.ChessRenderer;
import com.olsen.easychess.drawing.GameActivity;
import com.olsen.easychess.opencv.Opencv;
import com.olsen.easychess.utils.FileIOHelper;

import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

public class CameraActivity extends ActionBarActivity {

	private static final int REQUEST_IMAGE_CAPTURE = 1;
	private static final String TAG = "CameraActivity";

	//private ProgressBar mProgressBar;
	private Bitmap mBitmap;

	static {
		if (!OpenCVLoader.initDebug()) {
			// Handle initialization error
			Log.e(TAG, "Can't initialize OpenCV");
		} else {
			Log.i(TAG, "OpenCV loaded successfully");
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);

		//mProgressBar = (ProgressBar) findViewById(R.id.imageInProcessing);
		Opencv.init(this);
	}

	@Override
	protected void onResume() {
		Log.w(TAG, "Resume");
		super.onResume();
		
		//mProgressBar.setVisibility(mBitmap == null ? View.INVISIBLE : View.VISIBLE);
		
		if (mBitmap != null) {	 
			//File file = FileIOHelper.save(this, "photo/camera.png", mBitmap);
			File file = FileIOHelper.open(this, "board-set/DSC_0091.JPG");

			Mat image = Highgui.imread(file.getAbsolutePath());
			Chessboard<Chesspiece> board = Opencv.process(image);
			mBitmap = null;

			if (board == null) {
				showToast("Chessboard not recognized. You can try again.");
				dispatchTakePictureIntent();
			} else {
				Intent intent = new Intent(CameraActivity.this, GameActivity.class);
				
				ChessRenderer.Board = board;
				ChessRenderer.mHistory = new ArrayList<Chessboard<Chesspiece>>();
				ChessRenderer.mHistory.add((Chessboard<Chesspiece>) board.clone());
				ChessRenderer.mIndex = 0;
				
				startActivity(intent);
			}
		}else{
			dispatchTakePictureIntent();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.w(TAG, "onActivityResult");

		if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			mBitmap = (Bitmap) extras.get("data");
		} else if (requestCode == REQUEST_IMAGE_CAPTURE
				&& resultCode == RESULT_CANCELED) {
			mBitmap = null;
		}
	}

	private void dispatchTakePictureIntent() {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
		}
	}

	private void showToast(String message) {
		Context context = getApplicationContext();
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, message, duration);
		toast.show();
	}

}
