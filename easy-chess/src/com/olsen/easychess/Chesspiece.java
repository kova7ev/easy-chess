package com.olsen.easychess;

import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;

public class Chesspiece extends Cloneable {
	private ChesspieceType mType;
	private ChesspieceColor mColor;
	private Mat mMat;
	private static List<Chesspiece> mPieces;
	
	static{
		mPieces = new ArrayList<Chesspiece>();
		
		mPieces.add(new Chesspiece(ChesspieceType.Rook, ChesspieceColor.White));
		mPieces.add(new Chesspiece(ChesspieceType.Rook, ChesspieceColor.Black));
		mPieces.add(new Chesspiece(ChesspieceType.Knight, ChesspieceColor.White));
		mPieces.add(new Chesspiece(ChesspieceType.Knight, ChesspieceColor.Black));
		mPieces.add(new Chesspiece(ChesspieceType.Bishop, ChesspieceColor.White));
		mPieces.add(new Chesspiece(ChesspieceType.Bishop, ChesspieceColor.Black));
		mPieces.add(new Chesspiece(ChesspieceType.King, ChesspieceColor.White));
		mPieces.add(new Chesspiece(ChesspieceType.King, ChesspieceColor.Black));
		mPieces.add(new Chesspiece(ChesspieceType.Queen, ChesspieceColor.White));
		mPieces.add(new Chesspiece(ChesspieceType.Queen, ChesspieceColor.Black));
		mPieces.add(new Chesspiece(ChesspieceType.Pawn, ChesspieceColor.White));
		mPieces.add(new Chesspiece(ChesspieceType.Pawn, ChesspieceColor.Black));
	}

	public Chesspiece() {
	}
	
	public Chesspiece(ChesspieceType type, ChesspieceColor color){
		mType = type;
		mColor = color;
	}
	
	public ChesspieceType getType(){
		return mType;
	}
	
	public void setType(ChesspieceType type){
		mType = type;
	}
	
	public ChesspieceColor getColor(){
		return mColor;
	}
	
	public void setColor(ChesspieceColor color){
		mColor= color;
	}
	
	public Mat getMat(){
		return mMat;
	}
	
	public void setMat(Mat m){
		mMat = m;
	}
	
	public static List<Chesspiece> getAllPieces(){
		return mPieces;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Chesspiece)){
			return false;
		}
		Chesspiece piece = (Chesspiece) o;
		return getType() == piece.getType() && getColor() == piece.getColor();
	}
	
	@Override
	public int hashCode() {
		int result = 0;
		result = 37 * result + mColor.ordinal();
		result = 37 * result + mType.ordinal();
		return result;
	}
	
	@Override
	public String toString() {
		return String.format("%s(%s)", mType.name().substring(0, 4), mColor.name().substring(0, 1));
	}

	@Override
	public Object clone() {
		Chesspiece piece = new Chesspiece();
		piece.setColor(mColor);
		piece.setType(mType);
		piece.setMat(mMat == null ? null : mMat.clone());
		return piece;
	}
}
